"""
Userspace Linux Driver for the Logitech G13 Gamepad
"""

try:
    import os
    import sys
    import dbus
    import getopt
    import gobject
    import usb.core
    import dbus.service
    from evdev import ecodes
    from time import strftime
    from dbus.mainloop.glib import DBusGMainLoop
    from g13.g13device import G13Device
except ImportError, err:
    print('Missing Dependencies:')
    print(err)
    sys.exit(1)


class G13Driver(dbus.service.Object):
    """
    G13Driver Management Class

    :return:
    """

    # G13Driver Attributes
    svc_name = 'com.DavidCDean.G13Driver'
    bus_name = '/com/DavidCDean/G13Driver'

    def __init__(self):
        keymap = None
        self.silent = False
        
        # Check for command line arguments
        try:
            short_codes = "hsk:"
            long_codes = ["help", "silent", "keymap="]
            opts, leftover_args = getopt.getopt(sys.argv[1:], short_codes, long_codes)
            if len(leftover_args) > 0:
                self.print_handle("Unrecognized argument(s): %s" % str(leftover_args))
        except getopt.GetoptError as opterr:
            self.usage()
            print(opterr)
            sys.exit(1)
        
        # Iterate through valid cli args and handle them
        for flag, val in opts:
            if flag in ("-h", "--help"):
                self.usage()
                sys.exit(0)
            elif flag in ("-s", "--silent"):
                self.silent = True
            elif flag in ("-k", "--keymap"):
                keymap = val
                
        # We must have a keymap. 
        if keymap is None:
            self.usage()
            print('Keymap is required.')
            sys.exit(1)

        # Enable Dbus Service
        self.service_dbus()

        # Look for a device...
        self.g13device = None
        self.has_kernel_module = False
        if self.capture_device() is False:
            print('No devices found.')
            sys.exit(1)
        
        # Bind a keymap
        if self.bind_keymap(keymap) is None:
            print('Failed to bind keymap.')
            sys.exit(1)
        
        # MainLoop Init and Start
        gobject.threads_init()
        loop = gobject.MainLoop()
        context = loop.get_context()
        self.start_monitor()
        while True:
            context.iteration(True)


    def service_dbus(self):
        """
        Wire up for dbus service.
        :return:
        """
        DBusGMainLoop(set_as_default=True)
        b_path = dbus.service.BusName(self.svc_name, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, b_path, self.bus_name)

    
    def capture_device(self):
        """
        Find and Capture a G13 Device

        :return:
        """
        self.g13device = usb.core.find(idVendor=G13Device.VENDOR_ID,
                                       idProduct=G13Device.PRODUCT_ID)

        if self.g13device is not None:
            self.print_handle("Logitech G13 Device Found.")
            self.g13device.__class__ = G13Device
            self.g13device.__init__()

            # Check for an attached kernel driver
            if self.g13device.is_kernel_driver_active(0):
                self.print_handle('Detaching Kernel Driver.')
                try:
                    # If attached, attempt to detach
                    self.g13device.detach_kernel_driver(0)
                    self.has_kernel_module = True
                except usb.core.USBError as kernel_exception:
                    print("Failed to detach kernel driver.")
                    print(kernel_exception)
                    sys.exit(1)
            
            # Claim device interface so we can work with it 
            try:
                self.g13device.set_configuration()
                usb.util.claim_interface(self.g13device, 0)
                self.print_handle('Device Captured.')
                return True
            except usb.core.USBError as usb_claim:
                print("Failed to capture device.")
                print(usb_claim)
                sys.exit(1)
        else:
            # No devices found
            return False
        
    @dbus.service.method('com.DavidCDean.G13Driver',
                         in_signature='s', out_signature='b')
    def bind_keymap(self, keymap=None):
        if keymap is not None:
            # Fix this to handle outside keybind
            return self.g13device.bind_keymap(keymap)
        else:
            return None


    @dbus.service.method('com.DavidCDean.G13Driver',
                         in_signature='', out_signature='b')
    def start_monitor(self):
        """
        Starts the monitoring thread on the currently captured
        g13 device.

        Also exposed as dbus service method.

        :return:
        """
        if self.g13device is not None:
            self.g13device.start_monitor()
            self.print_handle('Device monitoring started.')
            return True
    
    @dbus.service.method('com.DavidCDean.G13Driver',
                         in_signature='', out_signature='b')
    def stop_monitor(self):
        """
        Stops the monitoring thread on the currently captured
        g13 device. Exposed as dbus service method.

        :return:
        """
        self.g13device.stop_monitor()
        self.print_handle('Device monitoring stopped.')
        return True
                 
    def print_handle(self, message):
        """
        Handles non-exception output. Behavior is defined by
        the cli "silent" argument.

        :param message:
        """
        if not self.silent:
            print("%s - %s" % (strftime("%H:%M:%S"), message))
    
    @staticmethod
    def usage():
        """
        Presents the standard usage statement.
        """
        print('Usage: g13driver.py [ -k <path_to_keymap> [-s] | -h ]')
        print('Arguments:')
        print(' -k \t --keymap \t\t Path to keymap file - Required')
        print(' -h \t --help \t\t Show this message')
        print(' -s \t --silent \t\t Silence informational console output')
        
    @dbus.service.method('com.DavidCDean.G13Driver')
    def driver_exit(self):
        """
        Begins teardown of any monitoring threads, attempts to
        to reattach the kernel driver if originally set, and exits.
        Exposed as dbus service method.
        """

        self.print_handle("Stopping Monitor.")
        self.stop_monitor()

        if self.has_kernel_module:
            try:
                self.print_handle("Restoring Kernel Module.")
                self.g13device.attach_kernel_driver(0)
            except usb.core.USBError as kernel_attach:
                print("Could not reattach kernel driver.")
                print(kernel_attach)
                sys.exit(1)
        sys.exit(0)

    # End G13Driver Class
    
# Entry Point 
if __name__ == "__main__":
    driver = G13Driver()  # pylint: disable-msg=C0103