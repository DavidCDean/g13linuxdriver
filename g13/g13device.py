try:
    import os
    import sys
    import usb.core
    from evdev import ecodes
    from g13monitor import G13Monitor
    from ConfigParser import ConfigParser
except ImportError, err:
    print('Missing Imports.')
    print(err)
    sys.exit(1)


#noinspection PyMissingConstructor
class G13Device(usb.core.Device):
    
    # G13 Device Attributes
    VENDOR_ID = 0x046d
    PRODUCT_ID = 0xc21c
    read_endpoint = 0
    write_endpoint = 1
    keys_enum = ("G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9",
                 "G10", "G11", "G12", "G13", "G14", "G15", "G16", "G17",
                 "G18", "G19", "G20", "G21", "G22", "UNDEF1", "LIGHT",
                 "SELECT", "L1", "L2", "L3", "L4", "M1", "M2", "M3",
                 "MR", "LEFT", "DOWN", "TOP", "UNDEF2", "LIGHT1", "LIGHT2",
                 "TOGGLE")

    def __init__(self):
        self.device_monitor = None
        self.monitor_active = False
        self.key_map = None
        self.keys_loaded = False

    def start_monitor(self):
        if self.keys_loaded:
            self.device_monitor = G13Monitor(self)
            self.device_monitor.start()
            self.monitor_active = True

    def stop_monitor(self):
        self.device_monitor.stop()
        self.monitor_active = False
        self.device_monitor = None
    
    def clear_key_map(self):
        self.key_map = [[[None, False] for bit in range(0, 8)] for byte in range(0, 5)]
        self.keys_loaded = False
        
    def load_keymap(self, user_keys=None, joystick_aboslute=True):
        # Iterate through userkeys list
        if user_keys is None:
            self.keys_loaded = False
            return False
        else:
            self.clear_key_map()
            self.key_map = user_keys
            self.keys_loaded = True
            return True

    def bind_keymap(self, keymap=None):
        """
        Processes a file path for g13->keyboard key bindings on the
        currently bound G13 device. Exposed as dbus service method.

        :param keymap:
        :return:
        """

        # Check if keymap, g13device, and file exist
        if (keymap is not None) and (os.path.exists(keymap)):

            g13section = "G13Driver"
            user_config = ConfigParser(allow_no_value=True)
            user_config.read(keymap)

            # Check for section
            if user_config.has_section(g13section):

                # Create empty user_keys (5 bytes, 8 bit slots each)
                user_keys = [[[None, False] for bit in range(0, 8)] for byte in range(0, 5)]

                # Set values for each key
                for key_name in G13Device.keys_enum:

                    # Walk through required keys, fetching values
                    key_index = G13Device.keys_enum.index(key_name)
                    byte_index = (key_index / 8)
                    bit_index = key_index % 8
                    key_val_state = list()

                    if user_config.has_option(g13section, key_name):
                        key_value = user_config.get(g13section, key_name)
                        if key_value in ecodes.__dict__:
                            key_val_state = [user_config.get(g13section, key_name), 0]
                        else:
                            key_type = self.get_key_type(key_value)
                            if key_type == 1:
                                key_val_state = [user_config.get(g13section, key_value), key_type]
                            elif key_type == 2:
                                key_val_state = [[x.strip() for x in user_config.get(g13section, key_value).split(',')], key_type]
                            else:
                                key_val_state = [None, 0]
                    else:
                        key_val_state = [None, 0]

                    user_keys[byte_index][bit_index] = key_val_state

                # Load user_keys to device
                if self.load_keymap(user_keys):
                    return True

                if self.device_monitor is not None:
                    self.device_monitor.no_listen = False
        return False

    def get_key_type(self, key_value):
        if key_value[:11] == "G13PROFILE_":
            return 1
        elif key_value[:9] == "G13MACRO_":
            return 2
        else:
            return None
