try:
    import sys
    import threading
    from evdev import UInput, ecodes as e
except ImportError, err:
    print('Missing Imports.')
    print(err)
    sys.exit(1)


class G13Monitor(threading.Thread):

    def __init__(self, device):
        threading.Thread.__init__(self)
        self.device = device
        self.last_bytes = [0, 0, 0, 0, 0, 0, 0, 0]
        self.finished = threading.Event()
        self.input_device = UInput()
        self.rebind = None

    def stop(self):
        self.finished.set()
        self.join()
        
    def read_data(self, endpoint):
        try:
            data = self.device.read(endpoint.bEndpointAddress, endpoint.wMaxPacketSize)
            if data is not None:
                return data
        except Exception as NoDataException:
            return None
               
    def run(self):
        endpoint = self.device[0][(0, 0)][self.device.read_endpoint]

        # Run Monitor Loop
        while not self.finished.isSet():

            # Check for rebind
            if self.rebind is not None:
                self.device.bind_keymap(self.rebind)
                self.rebind = None

            # Try to read data from Device
            incoming_bytes = self.read_data(endpoint)

            # If there's incoming data
            if incoming_bytes is not None:
                self.process_data(incoming_bytes)

    def process_data(self, incoming_bytes):

        # Loop through bytes 3-7 -> All regular keys
        for byte_index in range(3, 8):

            current_byte = incoming_bytes[byte_index]
            previous_byte = self.last_bytes[byte_index]
            change_byte = current_byte ^ previous_byte

            # Only work on this byte if something has changed
            if change_byte != 0:

                # Check each bit in a changed byte for processing
                for bit_index in range(0, 8):
                    if change_byte & (1 << bit_index) != 0:

                        # Determine Pressed or Not
                        if (current_byte & (1 << bit_index)) != 0:
                            pressed = True
                        else:
                            pressed = False

                        # Process the key change
                        self.process_keychange(byte_index - 3, bit_index, pressed)

        # Current Bytes become Previous Bytes
        self.last_bytes = incoming_bytes

    def process_keychange(self, byte_index, bit_index, pressed):
            try:
                key_value = self.device.key_map[byte_index][bit_index]
            except:
                print("Error getting key_value.")

            if key_value[0] is not None:
                if key_value[1] == 0:
                    self.input_device.write(e.EV_KEY, e.ecodes[key_value[0]], pressed)
                    self.input_device.syn()
                elif key_value[1] == 1 and pressed:
                    self.rebind = (key_value[0])
                elif key_value[1] == 2:
                    for action in key_value[0]:
                        # Fix this to handle up/downs
                        self.input_device.write(e.EV_KEY, e.ecodes[action], 1)
                        self.input_device.write(e.EV_KEY, e.ecodes[action], 0)
                    self.input_device.syn()
