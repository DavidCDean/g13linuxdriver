import dbus


'''
Ugly dbus test client. Used to test/demo exposed methods of g13driver.
'''

class dbus_test_client(object):
    def do_connect(self):
        try:
            # Hook up to dbus session
            bus_name = 'com.DavidCDean.G13Driver'
            bus = dbus.SessionBus()
            g13_driver = bus.get_object(bus_name, '/com/DavidCDean/G13Driver')
        
            # Map a few functions
            self.driver_exit = g13_driver.get_dbus_method('driver_exit', bus_name)
            self.stop = g13_driver.get_dbus_method('stop_monitor', bus_name)
            self.start = g13_driver.get_dbus_method('start_monitor', bus_name)
            self.keymap = g13_driver.get_dbus_method('bind_keymap', bus_name)
        except:
            pass
        
    def runstart(self):
        run = True
        while run:
            invar = raw_input("Enter \"start\", \"stop\", \"kill\", \"connect\", \"keymap\", or \"end\": ")
            try:
                if invar == "start":
                    self.start()
                elif invar == "stop":
                    self.stop()
                elif invar == "kill":
                    self.driver_exit()
                elif invar == "connect":
                    self.do_connect()
                elif invar == "keymap":
                    self.keymap('/home/david/workspace/G13LinuxDriver/g13keymaps/default.keys')
                elif invar == "end":
                    run = False
            except:
                print("Doh.")
                pass

if __name__ == "__main__":
    test = dbus_test_client()
    test.do_connect()
    test.runstart()
    