import usb.core
import usb.util


'''
Ugly usb test client. Used to test/demo reads against a g13 device.
'''

device = usb.core.find(idVendor=0x046d, idProduct=0xc21c)

if device is None:
    raise ValueError('Device not found.')
else:
    print('Logitech, Inc. G13 Advanced Gameboard Found.')
 
print('Device Class: %s' % device.bDeviceClass)

for cfg in device:
    print('Configuration Value: %s' % cfg.bConfigurationValue)
    for intf in cfg:
        print('Interface Number: %s \t Alternate: %s' % (intf.bInterfaceNumber, intf.bAlternateSetting))



if device.is_kernel_driver_active(0):
    print('Detatching Kernel Driver.')
    device.detach_kernel_driver(0)

device.set_configuration(1)

usb.util.claim_interface(device,0)
print('Device Captured.')


ReadEndpoint = device[0][(0,0)][0]
WriteEndpoint = device[0][(0,0)][1]

data = None

while 1:
    try:
        data = device.read(ReadEndpoint.bEndpointAddress,
                           ReadEndpoint.wMaxPacketSize)
        if data is not None:
                xaxis = "{0:08b}".format(data[1])
                yaxis = "{0:08b}".format(data[2]).zfill(8)
                col3 = "{0:08b}".format(data[3]).zfill(8)
                col4 = "{0:08b}".format(data[4]).zfill(8)
                col5 = "{0:08b}".format(data[5]).zfill(8)
                col6 = "{0:08b}".format(data[6]).zfill(8)
                col7 = "{0:08b}".format(data[7] % 128).zfill(8)
                print ("X: %s  Y: %s  C3: %s  C4: %s  C5: %s  C6: %s  C7: %s" % (xaxis, yaxis, col3, col4, col5, col6, col7))
    except usb.core.USBError as e:
        continue


